class PostsController < ApplicationController
	before_action :authenticate_user!
 	before_action :set_post, only: [ :show, :edit, :update, :destroy]
 
 	def index
 		@posts = Post.all
 	end

 	def wear
 		@posts = Post.wear
 	end

 	def eat
 		@posts = Post.eat
 	end

 	def live
 		@posts = Post.live
 	end

 	def culture
 		@posts = Post.culture
 	end

 	def show
 	end
 
 	def new
 		@post = Post.new
 	end
 
 	def create
 		@post = Post.new(post_params)
 		@post.user = current_user
 		if @post.save
 			redirect_to @post
 		else
 			render 'new'
 		end
 	end
 
 	def update
 		if @post.update(post_params)
 			redirect_to @post
 		else
 			render 'edit'
 		end
 	end
 
 	def destroy
 		@post.destroy
 		redirect_to posts_path
 	end
 
 	private
 
 	def post_params
 		params.require(:post).permit(:title,:content,:image,:category,:season)
 	end
 
 	def set_post
 		@post = Post.find(params[:id])
 	end
end
