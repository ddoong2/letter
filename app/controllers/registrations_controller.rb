class RegistrationsController < Devise::RegistrationsController
  
 # def new
 # 	@register = Register.new(sign_up_params)
 # 	@register.is_editor = 0
 	#render 'new'
  #end

  private

  def sign_up_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :is_editor)
    #params[:is_editor] = 1
  end

  def account_update_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :current_password)
  end
end