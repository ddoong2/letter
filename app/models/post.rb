class Post < ApplicationRecord
  belongs_to :user
  has_many :archiving_users, :through => :archives, class_name:"User"
  enum category: [ :wear, :eat, :live, :culture]
  enum season: [ :spring, :summer, :autumn, :winter]
end
