Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  
  devise_for :users, controllers: { registrations: 'registrations' }
  
  root 'login#welcome'
  
  resources :posts do
  	collection do
  		get :wear
  		get :eat
  		get :live
  		get :culture
  	end
  end

  post 'posts/wear'
  post 'posts/eat'
  post 'posts/live'
  post 'posts/culture'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
